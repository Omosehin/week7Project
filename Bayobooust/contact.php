<!DOCTYPE html>
<html>
<head>
    <title>PHP contact Page</title>

    <meta name="viewport" content="width=device-width", initial-scale="1">
    <style>
    body {
        font-family: Arial, Helvetica, sans-serif; 
    }
    input[type=text],input[type=email],select,textarea {
        width: 100%;
        padding:12px;
        border:1px solid black;
        border-radius: 4px;
        box-sizing:border-box;
        margin-top: 6px;
        margin-buttom: 16px;
        resize: vertical;
    }
    input[type=submit] {
        background-color: #3779c4;
        padding:12px 20px;
        border:none;
        border-radius: 4px;
        box-sizing:border-box;
        cursor: pointer;
        color: white;
    }
    input[type=submit]:hover {
        background-color: #45a049;
    }
    .container {
        border-radius: 5px;
        background-color: pink;
        padding: 20px;
    }
    .question {
        color: #28588e;
    }
    </style>
</head>

<body>

<?php if ( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) && isset($_POST['submit']) ): ?>

<?php

include "bayo.php";

bayo::process($_POST);

?>

<div class="container" align="center">
    <h1>Thank You for Contacting Us!</h1>
    <p>An email has been forwarded to you</p>
    <br>
    <a href="contact.php">Contact Us</a>
</div>

 <?php else: ?>

    <div class="container">
        <h1>Contact Us</h1>
        <form action="contact.php" method="POST">
            <label for="name">NAME</label>
            <input type="text" id="name" name="name" placeholder="Full Name" required>

            <label for="email">EMAIL</label>
            <input type="email" id="email" name="email" placeholder="Enter your Email Address">

            <label for="message">MESSAGE</label>
            <textarea id="message" name="message" rows="5"></textarea>

            <input type="submit" value="submit" name="submit">
        </form>
    </div>
<?php endif; ?>